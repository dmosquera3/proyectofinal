function textTure(ts,fw,fs,ff,c,t,x,y){

    var canvas = document.createElement('canvas');
    canvas.width = ts; canvas.height = ts;
    var context = canvas.getContext('2d');
    context.font = fw+" "+fs+" "+ff;
    context.fillStyle = "rgba("+c+")";
    context.fillText(t, x, y);
    
    // canvas contents will be used for a texture
    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    return texture;

}

function twoPointsDistance(a,b){//d = b-a
    return new THREE.Vector3(b.x-a.x,b.y-a.y,b.z-a.z);
}
function modulo(a){
    return Math.sqrt( (a.x*a.x) + (a.y*a.y) + (a.z*a.z) );
}
function unitario(a){
    var m = modulo(a);
    return new THREE.Vector3((a.x/m) , (a.y/m) , (a.z/m));
}
function randomHexColor(){
    return '#'+Math.floor(Math.random()*16777215).toString(16);
}
function parsePromtParams(string,flag){
    value = string.split(flag);
    for (var i = 0; i < value.length; i++) {
        if(i != value.length-1) value[i] = parseFloat(value[i]);
    };
    return value;
}

function crearCuboInterface(){

    var message = "Que viva el prompt carajo!";
    var params = prompt(message,"w,h,d,ws,hs,ds,#ffffff");

    if(params){

        var values = parsePromtParams(params,",");
        var w = values[0] || 10,
        h = values[1] || 10,
        d = values[2] || 10,
        ws = values[3] || 1,
        hs = values[4] || 1,
        ds = values[5] || 1;
        var color = values[6] || randomHexColor();

        return crearCubo(w,h,d,ws,hs,ds,color);
    }
    return false;
}

function crearCubo(w,h,d,ws,hs,ds,color){
    var geometria = new THREE.CubeGeometry(w,h,d,ws,hs,ds);
    var material = new THREE.MeshBasicMaterial({
        color: color, wireframe: true
    });
    var cubo = new THREE.Mesh( geometria, material);
    objetos.push(cubo);
    scene.add(cubo);

    var obj = {
        id: objetos.length-1,
        name: "Cube | Obj "+objetos.length
    };
    console.log(color);
    console.log(cubo);
    return obj;
}
function paintFace3(geometry,faces){
    for (var i = 0; i < faces; i++) {
        var color = randomHexColor();
        geometry.faces[i].vertexColors[0] = new THREE.Color(color);
        geometry.faces[i].vertexColors[1] = new THREE.Color(color);
        geometry.faces[i].vertexColors[2] = new THREE.Color(color);
    };
    return geometry;
}


function getHeightData(img,scale) {

 if (scale == undefined) scale=1;

 var canvas = document.createElement( 'canvas' );
 canvas.width = img.width;
 canvas.height = img.height;
 var context = canvas.getContext( '2d' );

 var size = img.width * img.height;
 var data = new Float32Array( size );

 context.drawImage(img,0,0);

 for ( var i = 0; i < size; i ++ ) {
    data[i] = 0
}

var imgd = context.getImageData(0, 0, img.width, img.height);
var pix = imgd.data;

var j=0;
for (var i = 0; i<pix.length; i +=4) {
    var all = pix[i]+pix[i+1]+pix[i+2];
    data[j++] = all/(12*scale);
}

return data;
}
function loadModel(model,texture){
    var loader = new THREE.JSONLoader(); 
    loader.load( model,function(geometry,materials){  
        console.log(texture);
        for ( var i = 0; i < materials.length; i++ ) {
          materials[ i ].morphTargets = true;
          materials[i].map=new THREE.ImageUtils.loadTexture(model+".jpg");
          materials[i].lightMap=null;
      }

      material = new THREE.MeshFaceMaterial( materials );
      modelo3d_ = new THREE.SkinnedMesh ( geometry,material );  
      
      modelo3d_.castShadow = true;
      modelo3d_.receiveShadow = true;
      modelo3d_.scale.set(100,100,100);  
      modelo3d_.position.set(0,0,0);    

      scene.add(modelo3d_);
      
  });
}

function teclaPulsada(e)
{

    switch (e.keyCode)
    {
        case 37: // Izquierda
        case 65: // A
        TECLA.IZQUIERDA=true;
        break;
        case 39: // Derecha
        case 68: //D
        TECLA.DERECHA=true;
        break;
        case 38: // Arriba
        case 87: //W
        TECLA.ARRIBA=true;
        break;
        case 40: // Abajo
        case 83: //S
        TECLA.ABAJO=true;
        break;
        case 13: //Enter
        toggleFullScreen();
        break;
        case 73: //I
        printObjsInfo();
        break;
        case 48: //0

        break;
        case 49: //1
        camaraActiva = camaras.interactive;
        break;
        case 50: //2
        camaraActiva = camaras.tracking;
        break;
        case 51: //3
        camaraActiva = camaras.fixed;
        break;
        case 52: //4
        camaraActiva = camaras.tpersona;
        break;
    }

}
function teclaSoltada(e)
{
    switch (e.keyCode)
    {
        case 37: // Izquierda
        case 65: // A
        TECLA.IZQUIERDA=false;
        break;
        case 39: // Derecha
        case 68: //D
        TECLA.DERECHA=false;
        break;
        case 38: // Arriba
        case 87: //W
        TECLA.ARRIBA=false;
        break;
        case 40: // Abajo
        case 83: //S
        TECLA.ABAJO=false;
        break;
    }
}
function degToRad(degrees) {
    return degrees * Math.PI / 180;
}

function rotateAroundWorldAxis( object, axis, radians ) {
    var rotationMatrix = new THREE.Matrix4(); //Matriz identidad 4x4
    rotationMatrix.makeRotationAxis( axis.normalize(), radians );
    rotationMatrix.multiply( object.matrix );
    object.matrix = rotationMatrix;
    object.rotation.setFromRotationMatrix( object.matrix );
}

function rotateObjectsAroundWorldAxis( objects, axis, radians ) {
    for(var i=0; i<Object.keys(objects).length; i++)
    {
        var object = Object.keys(objects)[i];
        var rotationMatrix = new THREE.Matrix4(); //Matriz identidad 4x4
        rotationMatrix.makeRotationAxis( axis.normalize(), radians );
        rotationMatrix.multiply( objects[object].matrix );
        objects[object].matrix = rotationMatrix;
        objects[object].rotation.setFromRotationMatrix( objects[object].matrix );
    }
}

function moveObject(object , axis , distance){
    object.translateOnAxis(axis,distance);
}

function moveObjetcs(objects , axis , distance){
    for(var i=0; i<Object.keys(objects).length; i++)
    {
        var object = Object.keys(objects)[i];
        objects[object].translateOnAxis(axis,distance);
    }
}

function moveObjetcsByPivot(objects , axis , distance){
    for(var i=0; i<Object.keys(objects).length; i++)
    {
        var object = Object.keys(objects)[i];
        objects[object].pivot.translateOnAxis(axis,distance);
    }
}

function rotateObjectsAroundOwnAxis( objects, axis, radians ) {
    for(var i=0; i<Object.keys(objects).length; i++)
    {
        var object = Object.keys(objects)[i];
        var rotationMatrix = new THREE.Matrix4(); //Matriz identidad 4x4
        rotationMatrix.makeRotationAxis( axis.normalize(), radians );
        rotationMatrix.multiply( objects[object].pivot.matrix );
        objects[object].pivot.matrix = rotationMatrix;
        objects[object].pivot.rotation.setFromRotationMatrix( objects[object].pivot.matrix );
    }
}

