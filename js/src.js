var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var objetos = {};
var appW = window.innerWidth;
var appH = window.innerHeight;
var clock;
var TECLA = { ARRIBA:false, ABAJO:false, IZQUIERDA:false, DERECHA:false};
var cargador = {
	loadState: false,
	objsToLoad: 5,
	objsLoaded: 0,
	sceneIsReady: false,
	objReady: function(){
		this.objsToLoad--;
		this.objsLoaded++;
		var total = this.objsToLoad+this.objsLoaded;
		var porcentaje = (this.objsLoaded/total)*100;
		$("#barraDeCarga div#porcentaje").html(porcentaje+"%");
		$("#barraDeCarga div#carga").css("width",porcentaje+"%");
		if(this.objsToLoad == 0){
			this.loadState = true;
			$('#loader').slideUp(2000);
		}
	}
};


var cronometro_reloj= document.getElementById("cronometro")


var collidableMeshList = [];

function webGLStart(){

	clock = new THREE.Clock;

	iniciarEscena();
	ultiTiempo = Date.now();
	document.onkeydown = teclaPulsada;
	document.onkeyup = teclaSoltada;
	animarEscena();
}

function iniciarEscena(){
	
	//RENDERER
	renderer = new THREE.WebGLRenderer( {antialias: true} );
	renderer.setSize( appW , appH);
	renderer.setClearColorHex( 0xAAAAAA, 1.0);

	renderer.shadowMapEnabled = true;

	document.body.appendChild(renderer.domElement);

	//CAMARAS
	var view_angle = 60, aspect_ratio = appW/appH, near = 1, far = 20000;
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far);
	camera.position.set( 0,1000,400 );

	//CONTROL DE LA CAMARA
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement);


	//ESTADÍSTICAS
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '10px';
	stats.domElement.style.zIndex = '100';
	document.body.appendChild( stats.domElement );

	//Luces
	luzSpotLight = new THREE.SpotLight(0xFAAC58);
	luzSpotLight.position.set(0,1200,0);
	/*luzSpotLightNutro = new THREE.SpotLight(0xffffff);
	//luzSpotLightNutro.target.position.set(0,0,0);
	luzSpotLightNutro.shadowCameraVisible = false;
	luzSpotLightNutro.shadowDarkness = 0.6;
	luzSpotLightNutro.intensity = 1;
	luzSpotLightNutro.castShadow = true;
	luzSpotLightNutro.shadowMapWidth = luzSpotLightNutro.shadowMapHeight = 100;
	luzSpotLightNutro.shadowCameraNear = 1100;
	luzSpotLightNutro.shadowCameraFar = 1100;
	luzSpotLightNutro.shadowCameraFov = 20;
	luzSpotLightNutro.position.set(600,250,0);
	*/
	/*
	var currentmin=1;	
	var currentsec=59;	
	var currentmil=99;
	var keepgoin=false;	

	function timer(){
		if(keepgoin){
			currentmil-= 1;		
			if (currentmil==0){		
				currentmil=59;		
				currentsec-= 1;		
			}
			if (currentsec==0){		
				currentsec=59;		
				currentmin-= 1;		
			}
			Strsec=""+currentsec;		
			Strmin=""+currentmin;		
			Strmil=""+currentmil;		
			if (Strsec.length!=2){	
				Strsec="0"+currentsec;	
			}				
			if (Strmin.length!=2){	
				Strmin="0"+currentmin;
			}

			reloj.innerHTML=Strmin+" : "+Strsec+" : "+Strmil; 
			setTimeout("timer()", 100);	
		}
		function startover(){		
			keepgoin=false;			
			currentsec=1;
			currentmin=1;
			currentmil=1;
			Strsec="00";
			Strmin="00";
			Strmil="00";
		}

		*/

		bombilla = new THREE.Mesh(new THREE.SphereGeometry(1,1,1),new THREE.MeshBasicMaterial({color:0xffff00, wireframe:true}));
	//bombilla_1 = new THREE.Mesh(new THREE.SphereGeometry(1,1,1),new THREE.MeshBasicMaterial({color:0xffff00, wireframe:true}));

	//TERRENO
	var geometry = new THREE.PlaneGeometry(100,100,3,3);

	var material = new THREE.MeshLambertMaterial({color:0x8a8a8a, overdraw:true, side: THREE.DoubleSide, wireframe:true});

	mojojojo = new THREE.Mesh(geometry,material);
	mojojojo.rotation.x = -Math.PI/2;
	mojojojo.position.set(0,0,0);

	//collisionBox
	var geometry = new THREE.CubeGeometry(5,5,10,1,1,1);
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color:0x00ff00});
	materials.push(material);
	materials.push(wireframe);
	collisionBox = new THREE.Mesh(geometry,material);
	collisionBox.position.set(-15,2.5,0);
////////////////////////////////////////Carreteraaa//////////////////////////////////
var geometry = new THREE.PlaneGeometry(30,350,1,1,1);
	//var floorStreet = new THREE.ImageUtils.loadTexture( 'textures/street.jpg');
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color: 0x000000,side: THREE.BackSide, wireframe:false, opacity: 1});
	materials.push(material);
	materials.push(wireframe);
	PlaneStreet1 = new THREE.Mesh(geometry,material);
	PlaneStreet1.position.set(190,-10.5,-245);
	PlaneStreet1.rotation.x= -Math.PI/2
	PlaneStreet1.rotation.z= Math.PI/3.8;


	var geometry = new THREE.PlaneGeometry(30,350,1,1,1);
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color: 0x000000, side: THREE.BackSide, wireframe:false, opacity: 1});
	materials.push(material);
	materials.push(wireframe);
	PlaneStreet2 = new THREE.Mesh(geometry,material);
	PlaneStreet2.position.set(190,-13.7,50);
	PlaneStreet2.rotation.x= -Math.PI/2
	PlaneStreet2.rotation.z= -Math.PI/3.8;
/////////////////////////////////////////////////////CONTADOR///////////////////////////////////////////
/*contador tiempo//

var currentmin=59;	
var currentsec=59;	
var currentmil=99;
var keepgoin=false;		
function timer(){
	if(keepgoin){
		currentmil-= 1;		
		if (currentmil==0){		
			currentmil=59;		
			currentsec-= 1;		
		}
		if (currentsec==0){		
			currentsec=59;		
			currentmin-= 1;		
		}
		Strsec=""+currentsec;		
		Strmin=""+currentmin;		
		Strmil=""+currentmil;		
		if (Strsec.length!=2){	
			Strsec="0"+currentsec;	
		}				
		if (Strmin.length!=2){	
			Strmin="0"+currentmin;
		}

		reloj.innerHTML=Strmin+" : "+Strsec+" : "+Strmil; 
		setTimeout("timer()", 100);	
	}
}
function startover(){		
	keepgoin=false;			
	currentsec=1;
	currentmin=1;
	currentmil=1;
	Strsec="00";
	Strmin="00";
	Strmil="00";
}*/
/////////////////////////////////////////////////////////////////////////////////////////////////////
	//collisionBox2


	var geometry = new THREE.CubeGeometry(5,5,10,1,1,1);
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color:0x00ff00});
	materials.push(material);
	materials.push(wireframe);
	collisionBox2 = new THREE.Mesh(geometry,material);
	collisionBox2.position.set(-15,2.5,20);
	collisionBox2.rotation.y = Math.PI/2;

	var geometry = new THREE.CubeGeometry(200,200,50,1,1,1);
	var materials = [];
	var material = new THREE.MeshBasicMaterial({color: 0x000000,side: THREE.BackSide, wireframe:false, opacity: 1});
	materials.push(material);
	materials.push(wireframe);
	collisionBox3 = new THREE.Mesh(geometry,material);
	collisionBox3.position.set(109,2.5,-29);
	collisionBox3.rotation.x= -Math.PI/2
	collisionBox3.rotation.z= -Math.PI/3.8;
	
	//movingBox
	var geometry = new THREE.CubeGeometry(7,6,20,1,1,1);
	var materials = [];
	var wireframe = new THREE.MeshBasicMaterial({wireframe: false, opacity:0, trasparence: 1});
	materials.push(material);
	materials.push(wireframe);
	movingBox = new THREE.Mesh(geometry,wireframe);
	movingBox.position.set(50,-10.5,-500);

	///////////////////////////////PLANOS SKYBOX///////////////////////////////////////

	var geometry_Sky = new THREE.PlaneGeometry(1800,1800);
	var texture_Sky = new THREE.ImageUtils.loadTexture( 'textures/atardecer_montañas.jpg');
	var materials = [];
	var material = new THREE.MeshBasicMaterial({ map: texture_Sky, side: THREE.DoubleSide, ambient: 0, emissive: 0});
	materials.push(material);
	materials.push(wireframe);
	planeSkybox = new THREE.Mesh(geometry_Sky,material);
	planeSkybox.position.set(0,400,-800);
	//planeSkybox.rotation.x= -Math.PI/2

	var geometry_Sky_2 = new THREE.PlaneGeometry(1800,1800);
	var texture_Sky_2 = new THREE.ImageUtils.loadTexture( 'textures/atardecer_montañas_lado.jpg');
	var materials = [];
	var material = new THREE.MeshBasicMaterial({ map: texture_Sky_2, side: THREE.DoubleSide, ambient: 0, emissive: 0});
	materials.push(material);
	materials.push(wireframe);
	planeSkybox_2 = new THREE.Mesh(geometry_Sky_2,material);
	planeSkybox_2.position.set(900,400,100);
	planeSkybox_2.rotation.y= -Math.PI/2

	var geometry_Sky_3 = new THREE.PlaneGeometry(1800,1800);
	var texture_Sky_3 = new THREE.ImageUtils.loadTexture( 'textures/atardecer_montañas_lado.jpg');
	var materials = [];
	var material = new THREE.MeshBasicMaterial({ map: texture_Sky_3, side: THREE.DoubleSide, ambient: 0, emissive: 0});
	materials.push(material);
	materials.push(wireframe);
	planeSkybox_3 = new THREE.Mesh(geometry_Sky_3,material);
	planeSkybox_3.position.set(-900,400,100);
	planeSkybox_3.rotation.y= Math.PI/2


	var geometry_Sky_4 = new THREE.PlaneGeometry(1800,1800);
	var texture_Sky_4 = new THREE.ImageUtils.loadTexture( 'textures/atardecer_montañas.jpg');
	var materials = [];
	var material = new THREE.MeshBasicMaterial({ map: texture_Sky_4, side: THREE.DoubleSide, ambient: 0, emissive: 0});
	materials.push(material);
	materials.push(wireframe);
	planeSkybox_4 = new THREE.Mesh(geometry_Sky_4,material);
	planeSkybox_4.position.set(0,400,1000);
	planeSkybox.rotation.y= -Math.PI
//////////////////////////////////////////////PISO COLISIONABLE////////////////////////////////////////////////

var geometry = new THREE.PlaneGeometry(180,180,40,40);
var materials = [];
var wireframe = new THREE.MeshBasicMaterial({color: 0x000000, wireframe:false, opacity: 1});
materials.push(material);
materials.push(wireframe);
CollisionPlane = new THREE.Mesh(geometry,wireframe);
CollisionPlane.rotation.x = Math.PI/2;
CollisionPlane.scale.set(6,6,2);
CollisionPlane.position.set(-70,-14,0);
cargador.objReady();

var geometry = new THREE.PlaneGeometry(180,180,40,40);
var materials = [];
var wireframe = new THREE.MeshBasicMaterial({color: 0x000000,side: THREE.BackSide, wireframe:false, opacity: 1});
materials.push(material);
materials.push(wireframe);
winerPLane = new THREE.Mesh(geometry,wireframe);
winerPLane.rotation.y = Math.PI/2;
//winerPLane.scale.set(6,6,2);
winerPLane.position.set(-500,-14,400);
cargador.objReady();
/*
	var geometry = new THREE.CubeGeometry( 1000, 1000, 100);

	geometry.trasparence=0;

var Skybox_material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('textures/atardecer_montañas.jpg') } );

var face1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
var face2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
var face3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
var face4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
var face5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
var face6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

geometry.faceVertexUvs[0] = [];

geometry.faceVertexUvs[0][0] = [ face1[0], face1[1], face1[3] ];
geometry.faceVertexUvs[0][1] = [ face1[1], face1[2], face1[3] ];

geometry.faceVertexUvs[0][2] = [ face2[0], face2[1], face2[3] ];
geometry.faceVertexUvs[0][3] = [ face2[1], face2[2], face2[3] ];

geometry.faceVertexUvs[0][4] = [ face3[0], face3[1], face3[3] ];
geometry.faceVertexUvs[0][5] = [ face3[1], face3[2], face3[3] ];

geometry.faceVertexUvs[0][6] = [ face4[0], face4[1], face4[3] ];
geometry.faceVertexUvs[0][7] = [ face4[1], face4[2], face4[3] ];

geometry.faceVertexUvs[0][8] = [ face5[0], face5[1], face5[3] ];
geometry.faceVertexUvs[0][9] = [ face5[1], face5[2], face5[3] ];

geometry.faceVertexUvs[0][10] = [ face6[0], face6[1], face6[3] ];
geometry.faceVertexUvs[0][11] = [ face6[1], face6[2], face6[3] ];

Cubo = new THREE.Mesh(geometry,  Skybox_material);
Cubo.position.x = 1100;
Cubo.position.y= 50;
Cubo.position.z = 1100;

*/





////////////////////////////TERRENO/////////////////////////////////////////////
var img = new Image();

img.onload = function(){

	var data = getHeightData(img);

	geometry = new THREE.PlaneGeometry(180,180,199,199);
	texture = THREE.ImageUtils.loadTexture( './textures/jotunheimen-texture_21.jpg');
	material = new THREE.MeshLambertMaterial( {map: texture} );

	for (var i = 0; i < geometry.vertices.length; i++) {
		geometry.vertices[i].z = data[i];
	};

	pngTerrain = new THREE.Mesh( geometry, material );
	pngTerrain.rotation.x = -Math.PI/2;
	pngTerrain.scale.set(6,6,2);
	pngTerrain.position.set(-70,-15,0);


};

img.src = './terrenos/jotunheimen_2.png';

cargador.objReady();
	///////////////////////////LeCamaro///////////////////////////////////////////////////

	materialCamaro = {
		body: {
			Red: new THREE.MeshPhongMaterial({
				color: 0x660000,
				combine: THREE.MixOperation,
				reflectivity: 0.3,
				shininess: 200
			}),
			Blue: new THREE.MeshPhongMaterial({
				color: 0x226699,
				combine: THREE.MixOperation,
				reflectivity: 0.3,
				shininess: 200
			})
		},
		chrome: new THREE.MeshLambertMaterial({
			color:0xffffff
		}),
		darkchrome: new THREE.MeshLambertMaterial({
			color: 0x444444
		}),
		glass: new THREE.MeshBasicMaterial({
			color: 0x223344,
			opacity: 0.25,
			combine: THREE.MixOperation,
			reflectivity: 0.25,
			transparent: true
		}),
		tire: new THREE.MeshLambertMaterial({
			color: 0x050505
		}),
		interior: new THREE.MeshPhongMaterial({
			color: 0x050505,
			shininess: 20
		}),
		black: new THREE.MeshLambertMaterial({
			color: 0x000000
		})
	}

	var loader = new THREE.BinaryLoader();

	loader.load("models/camaro/CamaroNoUV_bin.js",function(geometry){
		
		var m = new THREE.MeshFaceMaterial();

		m.materials[0] = materialCamaro.body["Red"]; //Cuerpo del carro
		m.materials[1] = materialCamaro.chrome; // Cromo de las llantas
		m.materials[2] = materialCamaro.chrome; // grille chrome
		m.materials[3] = materialCamaro.darkchrome; // door lines
        m.materials[4] = materialCamaro.glass; // windshield
        m.materials[5] = materialCamaro.interior; // interior
        m.materials[6] = materialCamaro.tire; // tire
        m.materials[7] = materialCamaro.black; // tireling
        m.materials[8] = materialCamaro.black; // behind grille

        camaro = new THREE.Mesh(geometry,m);
        camaro.position.set(50,-10.5,-500);

        cargador.objReady();
        //(50,-10.5,-500)
    });

}

//////////////////////////////////SONIDOS////////////////////////////////////////


sonido1 = new Sound(['sounds/prender.mp3'],800,1);
	//sonido1.load();
	//sonido1.position.set(1100,0,1100);
	sonido1.play();

	sonido2 = new Sound(['sounds/arrancar.mp3'],800,1);
	//sonido2.load();
	sonido2.pause();

	sonido3 = new Sound(['sounds/prendido.mp3'],800,1);
	//sonido2.load();
	sonido3.pause();
	

//function RocaCube() {
 //Cube
//ancho altura profundida
var geometry = new THREE.CubeGeometry( 10, 25, 10,1,1,1);

geometry.trasparence=0;

var roca_Material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('textures/roca_cubo.jpg') } );

var face1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
var face2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
var face3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
var face4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
var face5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
var face6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

geometry.faceVertexUvs[0] = [];

geometry.faceVertexUvs[0][0] = [ face1[0], face1[1], face1[3] ];
geometry.faceVertexUvs[0][1] = [ face1[1], face1[2], face1[3] ];

geometry.faceVertexUvs[0][2] = [ face2[0], face2[1], face2[3] ];
geometry.faceVertexUvs[0][3] = [ face2[1], face2[2], face2[3] ];

geometry.faceVertexUvs[0][4] = [ face3[0], face3[1], face3[3] ];
geometry.faceVertexUvs[0][5] = [ face3[1], face3[2], face3[3] ];

geometry.faceVertexUvs[0][6] = [ face4[0], face4[1], face4[3] ];
geometry.faceVertexUvs[0][7] = [ face4[1], face4[2], face4[3] ];

geometry.faceVertexUvs[0][8] = [ face5[0], face5[1], face5[3] ];
geometry.faceVertexUvs[0][9] = [ face5[1], face5[2], face5[3] ];

geometry.faceVertexUvs[0][10] = [ face6[0], face6[1], face6[3] ];
geometry.faceVertexUvs[0][11] = [ face6[1], face6[2], face6[3] ];

Cubo = new THREE.Mesh(geometry,  roca_Material);
Cubo.position.x = -58;
Cubo.position.y= -5.9; 
Cubo.position.z = 48;


var geometry = new THREE.CubeGeometry( 10, 25, 10,1,1,1);

geometry.trasparence=0;

var roca_Material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('textures/roca_cubo.jpg') } );

var face1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
var face2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
var face3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
var face4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
var face5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
var face6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

geometry.faceVertexUvs[0] = [];

geometry.faceVertexUvs[0][0] = [ face1[0], face1[1], face1[3] ];
geometry.faceVertexUvs[0][1] = [ face1[1], face1[2], face1[3] ];

geometry.faceVertexUvs[0][2] = [ face2[0], face2[1], face2[3] ];
geometry.faceVertexUvs[0][3] = [ face2[1], face2[2], face2[3] ];

geometry.faceVertexUvs[0][4] = [ face3[0], face3[1], face3[3] ];
geometry.faceVertexUvs[0][5] = [ face3[1], face3[2], face3[3] ];

geometry.faceVertexUvs[0][6] = [ face4[0], face4[1], face4[3] ];
geometry.faceVertexUvs[0][7] = [ face4[1], face4[2], face4[3] ];

geometry.faceVertexUvs[0][8] = [ face5[0], face5[1], face5[3] ];
geometry.faceVertexUvs[0][9] = [ face5[1], face5[2], face5[3] ];

geometry.faceVertexUvs[0][10] = [ face6[0], face6[1], face6[3] ];
geometry.faceVertexUvs[0][11] = [ face6[1], face6[2], face6[3] ];

Cubo_1 = new THREE.Mesh(geometry,  roca_Material);
Cubo_1.position.x = -100;
Cubo_1.position.y= -5.9; 
Cubo_1.position.z = 150;


var geometry = new THREE.CubeGeometry( 10, 25, 10,1,1,1);

geometry.trasparence=0;

var roca_Material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('textures/roca_cubo.jpg') } );

var face1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
var face2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
var face3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
var face4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
var face5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
var face6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

geometry.faceVertexUvs[0] = [];

geometry.faceVertexUvs[0][0] = [ face1[0], face1[1], face1[3] ];
geometry.faceVertexUvs[0][1] = [ face1[1], face1[2], face1[3] ];

geometry.faceVertexUvs[0][2] = [ face2[0], face2[1], face2[3] ];
geometry.faceVertexUvs[0][3] = [ face2[1], face2[2], face2[3] ];

geometry.faceVertexUvs[0][4] = [ face3[0], face3[1], face3[3] ];
geometry.faceVertexUvs[0][5] = [ face3[1], face3[2], face3[3] ];

geometry.faceVertexUvs[0][6] = [ face4[0], face4[1], face4[3] ];
geometry.faceVertexUvs[0][7] = [ face4[1], face4[2], face4[3] ];

geometry.faceVertexUvs[0][8] = [ face5[0], face5[1], face5[3] ];
geometry.faceVertexUvs[0][9] = [ face5[1], face5[2], face5[3] ];

geometry.faceVertexUvs[0][10] = [ face6[0], face6[1], face6[3] ];
geometry.faceVertexUvs[0][11] = [ face6[1], face6[2], face6[3] ];

Cubo_2 = new THREE.Mesh(geometry,  roca_Material);
Cubo_2.position.x = 190;
Cubo_2.position.y= -5.9; 
Cubo_2.position.z = -245;


var geometry = new THREE.CubeGeometry( 10, 25, 10,1,1,1);

geometry.trasparence=0;

var roca_Material = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture('textures/roca_cubo.jpg') } );

var face1 = [new THREE.Vector2(0, .666), new THREE.Vector2(.5, .666), new THREE.Vector2(.5, 1), new THREE.Vector2(0, 1)];
var face2 = [new THREE.Vector2(.5, .666), new THREE.Vector2(1, .666), new THREE.Vector2(1, 1), new THREE.Vector2(.5, 1)];
var face3 = [new THREE.Vector2(0, .333), new THREE.Vector2(.5, .333), new THREE.Vector2(.5, .666), new THREE.Vector2(0, .666)];
var face4 = [new THREE.Vector2(.5, .333), new THREE.Vector2(1, .333), new THREE.Vector2(1, .666), new THREE.Vector2(.5, .666)];
var face5 = [new THREE.Vector2(0, 0), new THREE.Vector2(.5, 0), new THREE.Vector2(.5, .333), new THREE.Vector2(0, .333)];
var face6 = [new THREE.Vector2(.5, 0), new THREE.Vector2(1, 0), new THREE.Vector2(1, .333), new THREE.Vector2(.5, .333)];

geometry.faceVertexUvs[0] = [];

geometry.faceVertexUvs[0][0] = [ face1[0], face1[1], face1[3] ];
geometry.faceVertexUvs[0][1] = [ face1[1], face1[2], face1[3] ];

geometry.faceVertexUvs[0][2] = [ face2[0], face2[1], face2[3] ];
geometry.faceVertexUvs[0][3] = [ face2[1], face2[2], face2[3] ];

geometry.faceVertexUvs[0][4] = [ face3[0], face3[1], face3[3] ];
geometry.faceVertexUvs[0][5] = [ face3[1], face3[2], face3[3] ];

geometry.faceVertexUvs[0][6] = [ face4[0], face4[1], face4[3] ];
geometry.faceVertexUvs[0][7] = [ face4[1], face4[2], face4[3] ];

geometry.faceVertexUvs[0][8] = [ face5[0], face5[1], face5[3] ];
geometry.faceVertexUvs[0][9] = [ face5[1], face5[2], face5[3] ];

geometry.faceVertexUvs[0][10] = [ face6[0], face6[1], face6[3] ];
geometry.faceVertexUvs[0][11] = [ face6[1], face6[2], face6[3] ];

Cubo_3 = new THREE.Mesh(geometry,  roca_Material);
Cubo_3.position.x = 190;
Cubo_3.position.y= -13.7; 
Cubo_3.position.z = 50;

cargador.objReady();


//}

function llenarEscena() {

	//cronometro_reloj.style.visibility="visible";
	scene = new THREE.Scene();
	scene.fog = new THREE.Fog( 0x808080, 1000, 6000 );
	scene.add( camera );
	camera.lookAt(scene.position);
	//RocaCube();
	// Luces
	scene.add(new THREE.AmbientLight(0x050505));
	//scene.add(luzSpotLightNutro);
	scene.add(luzSpotLight);
	bombilla.position = luzSpotLight.position;
	scene.add(bombilla);
	//scene.add(bombilla_1);
	//bombilla_1.position=luzSpotLightNutro.position;

	//Elementos
	//scene.add(mojojojo);
	scene.add(PlaneStreet1);
	scene.add(PlaneStreet2);
	scene.add(camaro);
	//scene.add(collisionBox); 
	collidableMeshList.push(collisionBox);
	//scene.add(collisionBox2); 
	collidableMeshList.push(collisionBox2);
	scene.add(collisionBox3);
	scene.add(movingBox);
	collidableMeshList.push(CollisionPlane); collidableMeshList.push(Cubo);
	scene.add(pngTerrain); collidableMeshList.push(Cubo_1); collidableMeshList.push(Cubo_2);
	collidableMeshList.push(collisionBox3); collidableMeshList.push(Cubo_3);
	scene.add(planeSkybox); scene.add(planeSkybox_2);
	scene.add(planeSkybox_3); scene.add(planeSkybox_4);
	scene.add(CollisionPlane);
	scene.add( Cubo );
	scene.add(Cubo_1);
	scene.add(Cubo_2);
	scene.add(Cubo_3);
	scene.add(winerPLane); collidableMeshList.push(winerPLane);

	//scene.add( Cubo );
	
}

function animarEscena(){
	requestAnimationFrame( animarEscena );
	
	if(!cargador.loadState){
		console.log("Obj Loaded : "+cargador.objsLoaded+" / "+(cargador.objsToLoad+cargador.objsLoaded));
	}else{
		if(!cargador.sceneIsReady){
			llenarEscena();
			console.log("Obj Loaded: "+cargador.objsLoaded+" from "+(cargador.objsToLoad+cargador.objsLoaded));
			console.log("scene Ready");
			cargador.sceneIsReady = true;

		}
		renderEscena();
		actualizarEscena();
	}
}

function renderEscena(){
	renderer.render( scene, camera );
}

function actualizarEscena(){
	
	ultiTiempo = Date.now();
	controlCamara.update();

	var time = Date.now() *0.0005;
	var delta = clock.getDelta();
	var friccion = 0.3;
	var movSpeed = 0.7;
	var velocidad = 0;
	var manejabilidad = 0.03;
	aumentaVelocidad= false;
	difPos1= camaro.position.z - PlaneStreet1.position.z;
	difPos2= camaro.position.z - PlaneStreet2.position.z;
	//console.log(difPos);
	//console.log(camaro.position);
	//console.log(PlaneStreet1.position);
	

	if(TECLA.ARRIBA){
		
		sonido2.play();
		velocidad += (movSpeed*delta);
		moveObject(camaro , new THREE.Vector3(0,0,1) , movSpeed);
		moveObject(movingBox , new THREE.Vector3(0,0,1) , movSpeed);
		if (difPos1>=-37.71240460480408 && difPos1<= 121.5321582278515 || difPos2>= -113.75120805589071 && difPos2<= 61.35511216865467) {

			movSpeed+= friccion;
			velocidad += (movSpeed*delta);
			moveObject(camaro , new THREE.Vector3(0,0,1) , movSpeed);
			moveObject(movingBox , new THREE.Vector3(0,0,1) , movSpeed);
		}

	}else{
		//sonido1.pause();
		sonido2.pause();
		sonido3.play();
		velocidad = 0;
	}

	if(TECLA.ABAJO){
		moveObject(camaro , new THREE.Vector3(0,0,1) , -movSpeed/2);
		moveObject(movingBox , new THREE.Vector3(0,0,1) , -movSpeed/2);

	}
	if(TECLA.IZQUIERDA){
		rotateAroundWorldAxis(camaro , new THREE.Vector3(0,1,0) , manejabilidad);
		rotateAroundWorldAxis(movingBox , new THREE.Vector3(0,1,0) , manejabilidad);

	}
	if(TECLA.DERECHA){
		rotateAroundWorldAxis(camaro , new THREE.Vector3(0,1,0) , -manejabilidad);
		rotateAroundWorldAxis(movingBox , new THREE.Vector3(0,1,0) , -manejabilidad);
	}





/*
	var TeclaPul = false;
	var aceleration = 0.0; 

	if (TECLA.ARRIBA) {//Se usa la tecla arriba
	TeclaPul=true;//Boleano que detecta si se pisa el acelerador
		if (aceleration>=0 && aceleration<50){
		aceleration+=0.5;//La aceleración es constante hasta cierto valor
		PracticeBall.position.x= aceleration;
		//moveObject(PracticeBall , new THREE.Vector3(0,0,1) , movSpeed+aceleration);
		}
		if(aceleration>=50 && aceleration<100){
		aceleration+=0.25;//Entre más se acelere, lo hará con más lentitud
		PracticeBall.position.x= aceleration;
		//moveObject(PracticeBall , new THREE.Vector3(0,0,1) , movSpeed+aceleration);
		}
		if(aceleration>=100 && aceleration<120){
		aceleration+=0.0625;//Acelera más lento entre estos valores 
		PracticeBall.position.x= aceleration;
		//moveObject(PracticeBall , new THREE.Vector3(0,0,1) , movSpeed+aceleration);
		}
		if(aceleration>=120 && aceleration<130){
		aceleration+=0.03125;//Acelera más lento entre estos valores
		PracticeBall.position.x= aceleration;
		//moveObject(PracticeBall , new THREE.Vector3(0,0,1) , movSpeed+aceleration);
		}
		if(aceleration>=130 && aceleration<135){
		aceleration+=0.015625;//Valor máximo de aceleración en este caso 
		PracticeBall.position.x= aceleration;
		//moveObject(PracticeBall , new THREE.Vector3(0,0,1) , movSpeed+aceleration);
		}
		
		console.log("Usted va a "+aceleration+" km/h");
		}
		*/





		var originPoint = movingBox.position.clone();
	//var originPoint = camaro.position.clone();

	for (var vertexIndex = 0; vertexIndex < movingBox.geometry.vertices.length; vertexIndex++)
	{		
		var localVertex = movingBox.geometry.vertices[vertexIndex].clone();
		var globalVertex = localVertex.applyMatrix4( movingBox.matrix );
		var directionVector = globalVertex.sub( movingBox.position );
		
		var ray = new THREE.Raycaster( originPoint, directionVector.clone().normalize() );
		collisionResults = ray.intersectObjects( collidableMeshList );
		if ( collisionResults.length > 0 && collisionResults[0].distance < directionVector.length() ) {
			console.log('DAMN!');
			moveObject(movingBox , new THREE.Vector3(0,0,1) , -movSpeed/2);
			moveObject(camaro , new THREE.Vector3(0,0,1) , -movSpeed/2);
			seTocan= camaro.position.x-winerPLane.position.x;
			seTocan2= camaro.position.z -winerPLane.position.z;

			if (seTocan==0 && seTocan2 && collisionResults.length > 0) {


				var message = "Has llegado!!!!!!!";
				console.log(message+"Ganaste");
			};
		}

	}	

	stats.update();


}